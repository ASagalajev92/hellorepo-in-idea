package main;

import java.util.Scanner;

public class Variables {
    public static void main(String[] args) {

        //region double exeption
        //  Int, double , string
        double curency = 150;
        double garbage = 100.75;

        double remainder = curency - garbage;

        String remainderEur = remainder + "E";

        System.out.println(remainderEur);
        //endregion

        //region array exeption int []
        // Arrays
        int [] garbageArray = {10, 13, 56, 34, 86};
        garbageArray [2] = 64;

        System.out.println(garbageArray[2]);
        //endregion

        //region if exeption int[]
        //If
        int [] kiirus = {10,40,45,51,32,42};

        if (kiirus[0] > 50){
            System.out.println("You have fine");
        }
        else if (kiirus[1] == 40){
            System.out.println("Your speed is good");
        }
        else {
            System.out.println("Your speed is OK");
        }
        //endregion

        //region string example(if)
        //String cycle example
        String sihtpunkt = "Narva";

        if (sihtpunkt.equals("Tartu")){
            System.out.println("Turn right");
        } else if (sihtpunkt.equals("Viljandi")){
            System.out.println("Turn left");
        } else {
            System.out.println("Go stright");
        }
        //endregion

        //region while cycle(int)
        int l = 1 ;
        while (l < 4) {
            System.out.println("Number " + l);
            l = l + 1;
        }
        //endregion

        //region while+,int[],

        int[] speeds = {12, 4, 65, 7, 8, 140, 66, 24, 65};

        int i = 0;
        while (i < 8) {
            if (speeds[i] < 50) {
                System.out.println("You`r good guy !");
            } else {
                System.out.println("You must stop and pay !");
            }
            i = i +1;
        }
        System.out.println("Cicle ended");
        //endregion,

        //region Math.pow() method - vozvedenije 4isla v stepen . pow() vozvrawajet 4islo tipa DOUBLE.
        int fnumber = 4;
        int snumber = 7;

        double result = Math.pow(fnumber,snumber);

        System.out.println(result);
        //endregion

        //region Math.abs() - vqvodit zna4enije 4isla u4itivaja jego tip no ne u4itivaja - .
        int num1 = -8;
        double num2 = -53;

        System.out.println(Math.abs(num1));
        System.out.println(Math.abs(num2));
        //endregion

        //region Scanner() + next(),nextLine(),nextDouble()
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Whats Your name ? ");
        String name = keyboard.nextLine(); // nestLine - zapominajet vsju stroku . Prosto next do probela.
        System.out.println("Whats Your age ? ");
        int age = keyboard.nextInt(); // zapominajet vvodimqje celije 4isla.
        System.out.println("Whats Your salary ? ");
        double salary = keyboard.nextDouble(); // zapominajet vvodimqje 4isla s tipom double.

        System.out.println("Hi " + name + ". You`r age is " + age + ". And You`r salary is : " + salary);
        //endregion


        //region Scanner() + %s-string,%d-decimal,%f-float,%n-smena stroki - Nado ukazivat posle kazdogo printa SCANNER.
        Scanner keyboard2 = new Scanner(System.in);

        System.out.println("Write You`r name : ");
        String name2 = keyboard2.nextLine();
        int age2 = 29;
        double salary2 = 14902.0;

        System.out.printf("You`r name is %s , and age is %d , and You have %f salary. %n", name2, age2, salary2);
        //endregion


    }
        
        /*    Contr + Shift + / - Coments text inside   */
//        Contr + /  - Comments all line

}

